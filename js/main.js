'use strict';

/*global $*/

var AIA = AIA || {};

AIA.test = (function () {

	function featureBg() {
		var $imagecontainer = $('.feature-image'),
			$imageItem = $('.feature-image img').attr('src');

		$imagecontainer.css('background-image', 'url(' + $imageItem + ')');
	}

	function navigation() {
		$('.menu-button').on('click', function(){
			$('nav').slideToggle();
		});
		window.onresize = function() {
			if( $(window).width() > 783) {
			  $('nav').show();
			} 
		};
	}

    return {
    	featureBg: featureBg,
    	navigation: navigation
    };

}());


$(document).ready(function () {
	AIA.test.featureBg();
	AIA.test.navigation();
});