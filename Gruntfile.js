module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      dist: {
        options: {
          outputStyle: 'expanded',
          sourceMap: true,
          lineNumber: true
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }
      }
    },

    watch: {
      grunt: {
        options: {
          reload: true
        },
        files: ['Gruntfile.js']
      },

      sass: {
        files: 'scss/**/*.scss',
        tasks: ['sass'],
      }
    }

    // imagemin: {
    //   optimizationLevel: 7,
    //   progressive: true,
    //   interlaced: true,
    //   files: {
    //    'images/*.png': '*.png',
    //    'images/*.jpg': '*.jpg'
    //  }

    // }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-contrib-imagemin');

  grunt.registerTask('build', ['sass']);
  grunt.registerTask('default', ['build','watch']);
}
